# Documentation

## Structure du projet

- `data_lake`: Dossier pour stocker les fichiers du data lake.
- `data_warehouse`: Dossier pour stocker les scripts d'initialisation de la base de données PostgreSQL.
- `etl`: Dossier pour stocker le code de la pipeline ETL.
- `docker-compose.yml`: Configuration des services Docker.
- `requirements.txt`: Dépendances Python.
- `etl.py`: Code de la pipeline ETL.
- `left_join_query.sql`: Requête SQL de LEFT JOIN.

## Pipeline ETL

1. Extraction des données depuis `data_lake/raw_data.csv`.
2. Transformation des données :
   - Ajout d'une colonne "id" avec des valeurs uniques.
   - Ajout d'une colonne "description" (initialisée à vide).
   - Transformation des données (exemple: ajout d'une colonne).
   - Extraction de la date sans l'heure.
3. Chargement des données transformées dans PostgreSQL.

## Exécution avec Docker

1. Assurez-vous que Docker est installé.
2. Exécutez `docker-compose up` pour lancer la pipeline ETL.

## Requête SQL de LEFT JOIN

Consultez le fichier `left_join_query.sql` dans le dossier `data_warehouse`.

``` sql
SELECT
    ev.id,
    ev.evenement,
    ev.date,
    ev.description,
    c.name AS country_name
FROM
    event ev
LEFT JOIN
    country c ON ev.country_code = c.code;
```

## Instructions pour Exécuter le Projet

Exécutez la commande suivante pour démarrer les conteneurs Docker.
```bash
docker-compose up -d
```
Le script Python ETL sera automatiquement exécuté pour transformer et charger les données dans PostgreSQL.
Pour arrêter les conteneurs, utilisez la commande suivante.
```bash
docker-compose down
```

## Commandes pour consulter la BDD
```bash
psql -U myuser -d mydb -h localhost -W

\dt

SELECT * FROM country;
SELECT * FROM event;
```
