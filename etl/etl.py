import os
import time
from datetime import datetime
import pandas as pd
from sqlalchemy import create_engine
from sqlalchemy import String


def extract_data():
    # Extraction des données (exemple: CSV)
    data = pd.read_csv('/app/data_lake/events.csv')
    return data

def transform_data(data):
    # Ajout d'une colonne "id" avec des valeurs uniques
    data['id'] = range(1, len(data) + 1)

    # Transformation de la colonne de date
    data['date'] = pd.to_datetime(data['date'], format='%Y%m%d%H%M')
    data['date'] = data['date'].dt.strftime('%Y-%m-%d')

    # Ajout d'une colonne "description" (initialisée à vide)
    data['description'] = ''

    # Sélection des colonnes nécessaires
    transformed_data = data[['id', 'evenement', 'date', 'description', 'country_code']]

    return transformed_data

def load_events_data_to_postgres(data):
    # Chargement des données dans PostgreSQL
    engine = create_engine('postgresql://myuser:mypassword@postgres:5432/mydb')
    data.to_sql('event', engine, index=False, if_exists='append', method='multi')


def load_countries_data_to_postgres():
    # Création d'une table des pays dans PostgreSQL avec le code du pays comme clé primaire
    engine = create_engine('postgresql://myuser:mypassword@postgres:5432/mydb')
    
    countries_data = pd.read_csv('/app/data_lake/countries.csv')  # Chemin vers le fichier des pays 
    countries_data = countries_data[['code', 'name']]
    countries_data.to_sql('country', engine, index=False, if_exists='replace', dtype={'code': String(2)})


def create_tables():
    # Attente que PostgreSQL soit prêt
    max_retries = 10
    retry_interval = 5
    for _ in range(max_retries):
        try:
            engine = create_engine('postgresql://myuser:mypassword@postgres:5432/mydb')
            conn = engine.connect()
            conn.close()
            break
        except Exception as e:
            print(f"Error connecting to PostgreSQL: {e}. Retrying in {retry_interval} seconds...")
            time.sleep(retry_interval)
    else:
        print("Max retries reached. Exiting.")
        return

    # Lire et exécuter le script SQL pour créer la table country
    with open('/app/data_warehouse/init.sql', 'r') as file:
        sql_script = file.read()
    with engine.connect() as con:
        con.execute(sql_script)


def main():
    # create_countries_table()  # Création de la table des pays
    # load_countries_data_to_postgres()
    # create_events_table() # Création de la table des évènements
    # create_tables()
    raw_data = extract_data()
    transformed_data = transform_data(raw_data)
    load_events_data_to_postgres(transformed_data)



if __name__ == "__main__":
    main()

##################################################################################################
############################################### TEST #############################################
##################################################################################################
# def create_events_table():
#     engine = create_engine('postgresql://myuser:mypassword@postgres:5432/mydb')
#     # Supprimer la table existante
#     with engine.connect() as con:
#         con.execute('DROP TABLE IF EXISTS event;')
    
#     with engine.connect() as con:
#         con.execute('ALTER TABLE public.country ADD CONSTRAINT unique_country_code UNIQUE (code);')

#     # Création d'une table events dans PostgreSQL
#     with engine.connect() as con:
#         con.execute('''
#             CREATE TABLE event (
#                 id SERIAL PRIMARY KEY,
#                 Evenement VARCHAR(255),
#                 date DATE,
#                 description VARCHAR(255),
#                 country_code VARCHAR(2),
#                 FOREIGN KEY (country_code) REFERENCES country(code)
#             );
#         ''')

# def create_countries_table():
#     engine = create_engine('postgresql://myuser:mypassword@postgres:5432/mydb')

#     # Supprimer la table existante
#     with engine.connect() as con:
#         con.execute('DROP TABLE IF EXISTS country;')


#     # Création d'une table events dans PostgreSQL
#     with engine.connect() as con:
#         con.execute('''
#             CREATE TABLE country(
#                 code VARCHAR(2) PRIMARY KEY,
#                 name VARCHAR(255)
#             );
#         ''')
##################################################################################################