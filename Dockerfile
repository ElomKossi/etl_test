# Utiliser l'image de base Python
FROM python:3.8

# Définir le répertoire de travail dans le conteneur
WORKDIR /app

# Copier les fichiers nécessaires dans le conteneur
COPY data_warehouse/countries.csv ./data_warehouse/
COPY data_warehouse/init.sql ./data_warehouse/
COPY data_lake/events.csv ./data_lake/ 
COPY etl/requirements.txt ./etl/
COPY etl/etl.py ./etl/
# COPY etl/Dockerfile ./etl/
COPY etl/README.md ./etl/

# Installer les dépendances Python
RUN pip install --no-cache-dir -r etl/requirements.txt

# CMD pour exécuter la pipeline ETL lorsque le conteneur est lancé
CMD ["python", "etl/etl.py"]

