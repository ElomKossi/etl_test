SELECT
    ev.id,
    ev.evenement,
    ev.date,
    ev.description,
    c.name AS country_name
FROM
    event ev
LEFT JOIN
    country c ON ev.country_code = c.code;