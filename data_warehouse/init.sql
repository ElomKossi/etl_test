-- Table pour les pays
CREATE TABLE country (
    code VARCHAR(2) PRIMARY KEY,
    name VARCHAR(255) NOT NULL
);

-- Table pour les événements
CREATE TABLE event (
    id SERIAL PRIMARY KEY,
    Evenement VARCHAR(255),
    date DATE,
    description VARCHAR(255),
    country_code VARCHAR(2) REFERENCES country(code)
);

-- Données initiales pour la table country depuis un CSV
COPY country (name, code) FROM '/docker-entrypoint-initdb.d/countries.csv' DELIMITER ',' CSV HEADER;
